package RouterSpecs

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import entity.ListingEntity.{Listing, ListingCreatedResponse}
import entity.ReviewEntity
import entity.ReviewEntity._
import org.scalatest.Inside.inside
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.must.Matchers.include
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import router._
import ReviewRouterSpec.{instance, getRouter}
import scala.concurrent.{Await, Future}
import scala.util.Success

object ReviewRouterSpec extends MyTestkitSetup {
  def instance(id: String = "listingID"): ReviewInput = ReviewInput(
    listingId = id,
    reviewerId = "reviewerID",
    reviewerName = "Name",
    comment = "Comment"
  )
  def getRouter: ReviewRouter = {
    val clusterShardingStub: ClusterShardingStub = getStub
    new ReviewRouter(
      typedSystem,
      clusterShardingStub
    )
  }
}
class ReviewRouterSpec
    extends AnyFeatureSpec
    with GivenWhenThen
    with ScalatestRouteTest
    with SprayJsonSupport
    with ReviewFormats
    with BeforeAndAfterEach
    with MyTestkitSetup {

  def setupListing: Future[Listing] = {
    val listing = ListingRouterSpec.instance()
    for {
      getListing <- ListingRouterSpec.getRouter
        .createListing(listing)
        .mapTo[ListingCreatedResponse]
    } yield getListing.listing.get
  }

  def setupReview: Future[(Listing, Review)] = {
    for {
      listing <- setupListing
      getReview <- reviewRouter
        .createReview(instance(listing.id))
        .mapTo[ReviewAddedResponse]
    } yield (listing, getReview.review.get)

  }

  private var reviewRouter: ReviewRouter = _

  override def afterAll(): Unit = testKit.shutdownTestKit()

  Feature("Create a Review") {
    Scenario("Creating a Review with a nonexistent Listing") {
      Given("a new Review, with an incorrect ListingID")
      reviewRouter = getRouter
      val rev = instance()
      When(s"a create request is made for $rev")
      val r = Post("/review/", rev) ~> reviewRouter.routesReview
      Then("Bad Request should be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[String] should include("Listing can not be found")
      }
    }
    Scenario("Creating a Review with a deleted Listing") {
      Given("a new Review, with a ListingID for a deleted Listing")
      reviewRouter = getRouter
      val listing = Await.result(setupListing, timeout.duration)
      Await.result(
        ListingRouterSpec.getRouter.deleteListing(listing.id),
        timeout.duration
      )
      val rev = instance(listing.id)
      When(s"a create request is made for $rev")
      val r = Post("/review/", rev) ~> reviewRouter.routesReview
      Then("Bad Request should be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseAs[String] should include("Listing is no longer available")
      }
    }
    Scenario("Creating a Review successfully") {
      Given("a new Review")
      reviewRouter = getRouter
      val listing = Await.result(setupListing, timeout.duration)
      val rev = instance(listing.id)
      When(s"a create request is made for $rev")
      val r = Post("/review/", rev) ~> reviewRouter.routesReview
      Then("a Review should be created successfully")
      r ~> check {
        status shouldEqual StatusCodes.Created
        inside(responseAs[Review]) { case x: Review =>
          x.comment shouldEqual rev.comment
          x.reviewerName shouldEqual rev.reviewerName
        }
      }
    }
  }
  Feature("Read a Review") {
    Scenario("Requested a nonexistent Review") {
      Given("no Review")
      reviewRouter = getRouter
      When("a read request is made")
      val r = Get("/review/1234") ~> reviewRouter.routesReview
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested a deleted Review") {
      Given("a deleted Review")
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      Await.result(reviewRouter.deleteReview(id), timeout.duration)
      When(s"a read request is made for Review $id")
      val r = Get(f"/review/$id") ~> reviewRouter.routesReview
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested an existing Review") {
      Given("an existing Review")
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      When(s"a read request is made for Review $id")
      val r = Get(f"/review/$id") ~> reviewRouter.routesReview
      Then("that Review should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
        inside(responseAs[Review]) { case x: Review =>
          x.reviewerName shouldEqual review.reviewerName
        }
      }
    }
  }
  Feature("Update a Review's Comment") {
    Scenario("Review did not exist") {
      Given("no Review, but an updated comment for one")
      reviewRouter = getRouter
      val n = Comment("Updated Comment")
      When("a read request is made")
      val r = Put("/review/1234", n) ~> reviewRouter.routesReview
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Review was deleted") {
      Given(
        "a deleted Review,an updated comment for one"
      )
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      Await.result(reviewRouter.deleteReview(id), timeout.duration)
      val n = Comment("Updated Comment")
      When(s"an update request is made for Review $id")
      val r = Put(f"/review/$id", n) ~> reviewRouter.routesReview
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Review successfully updated") {
      Given("an existing,an updated value for one")
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      val n = Comment("Updated Comment")
      When(s"an update request is made for Review $id")
      val r = Put(f"/review/$id", n) ~> reviewRouter.routesReview
      Then("OK should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
      }
      And("The Review's comment should be updated")
      reviewRouter.getReview(id).onComplete {
        case Success(GetReviewResponse(Some(x: Review))) =>
          inside(x) { case review: ReviewEntity.Review =>
            review.comment shouldEqual n
          }
      }
    }
  }
  Feature("Delete a Review") {
    Scenario("Review did not exist") {
      Given("no Review")
      reviewRouter = getRouter
      When("a delete request is made")
      val r = Delete("/review/1234") ~> reviewRouter.routesReview
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Review was deleted") {
      Given("a deleted Review")
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      Await.result(reviewRouter.deleteReview(id), timeout.duration)
      When(s"a delete request is made for Review $id")
      val r = Delete(f"/review/$id") ~> reviewRouter.routesReview
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Review successfully deleted") {
      Given("a Review")
      reviewRouter = getRouter
      val (_, review) = Await.result(setupReview, timeout.duration)
      val id = review.id
      val r = Delete(f"/review/$id") ~> reviewRouter.routesReview
      Then("NoContent should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NoContent
      }
    }
  }
}
