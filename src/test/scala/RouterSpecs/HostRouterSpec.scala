package RouterSpecs

import RouterSpecs.HostRouterSpec.{getRouter, instance}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import entity.HostEntity
import entity.HostEntity.{GetHostResponse, Host}
import org.scalatest.Inside.inside
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import router._

import scala.concurrent.Await
import scala.util.Success

object HostRouterSpec extends MyTestkitSetup with HostFormats {
  def getRouter: HostRouter = {
    new HostRouter(
      typedSystem,
      getStub
    )
  }
  def instance(): HostInput = HostInput("Name", 1, superHost = false)
}

class HostRouterSpec
    extends AnyFeatureSpec
    with GivenWhenThen
    with ScalatestRouteTest
    with SprayJsonSupport
    with HostFormats
    with BeforeAndAfterEach
    with MyTestkitSetup {

  var hostRouter: HostRouter = _
  override def afterAll(): Unit = testKit.shutdownTestKit()

  Feature("Create a Host") {
    Scenario("Creating a Host") {
      Given("a new Host")
      hostRouter = getRouter
      val h = instance()
      When("a create request is made")
      val r = Post("/host/", h) ~> hostRouter.routesHost
      Then("a Host should be created successfully")
      r ~> check {
        status shouldEqual StatusCodes.Created
        inside(responseAs[Host]) {
          case Host(_, name, responseRate, superHost) =>
            name shouldEqual h.name
            responseRate shouldEqual h.responseRate
            superHost shouldEqual h.superHost
        }
      }
    }
  }
  Feature("Read a Host") {
    Scenario("Requested a nonexistent Host") {
      Given("no Host")
      hostRouter = getRouter
      When("a read request is made")
      val r = Get("/host/1234") ~> hostRouter.routesHost
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested a deleted Host") {
      Given("a deleted host")
      hostRouter = getRouter
      val h = instance()
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      Await.result(hostRouter.deleteHost(id), timeout.duration)
      When(s"a read request is made for host $id")
      val r = Get(f"/host/$id") ~> hostRouter.routesHost
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested an existing Host") {
      Given("an existing Host")
      hostRouter = getRouter
      val h = instance()
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      When(s"a read request is made for host $id")
      val r = Get(f"/host/$id") ~> hostRouter.routesHost
      Then("that Host should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
        inside(responseAs[Host]) {
          case Host(_, name, responseRate, superHost) =>
            name shouldEqual h.name
            responseRate shouldEqual h.responseRate
            superHost shouldEqual h.superHost
        }
      }
    }
  }
  Feature("Update a Host") {
    Scenario("Host did not exist") {
      Given("no Host, but an updated value for one")
      hostRouter = getRouter
      val n = instance().copy(name = "Updated")
      When("a read request is made")
      val r = Put("/host/1234", n) ~> hostRouter.routesHost
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Host was deleted") {
      Given("a deleted host,an updated value for one")
      hostRouter = getRouter
      val h = instance()
      val n = instance().copy(name = "Updated")
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      Await.result(hostRouter.deleteHost(id), timeout.duration)
      When(s"an update request is made for host $id")
      val r = Put(f"/host/$id", n) ~> hostRouter.routesHost
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Host successfully updated") {
      Given("an existing,an updated value for one")
      hostRouter = getRouter
      val h = instance()
      val n = instance().copy(name = "Updated")
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      When(s"an update request is made for host $id")
      val r = Put(f"/host/$id", n) ~> hostRouter.routesHost
      Then("OK should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
      }
      And("The host should be updated")
      hostRouter.getHost(id).onComplete {
        case Success(GetHostResponse(Some(x: Host))) =>
          inside(x) { case host: HostEntity.Host =>
            host shouldEqual n
          }
      }

    }
  }
  Feature("Delete a Host") {
    Scenario("Host did not exist") {
      Given("no Host")
      hostRouter = getRouter
      When("a delete request is made")
      val r = Delete("/host/1234") ~> hostRouter.routesHost
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Host was deleted") {
      Given("a deleted host")
      hostRouter = getRouter
      val h = instance()
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      Await.result(hostRouter.deleteHost(id), timeout.duration)
      When(s"a delete request is made for host $id")
      val r = Delete(f"/host/$id") ~> hostRouter.routesHost
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Host successfully deleted") {
      Given("a host")
      hostRouter = getRouter
      val h = instance()
      val id = Await
        .result(hostRouter.createHost(h), timeout.duration)
        .asInstanceOf[HostEntity.HostCreatedResponse]
        .host
        .get
        .id
      When(s"an update request is made for host $id")
      val r = Delete(f"/host/$id") ~> hostRouter.routesHost
      Then("NoContent should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NoContent
      }
    }
  }
}
