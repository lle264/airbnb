package RouterSpecs

import org.scalatest.Suites

class FullSuite
    extends Suites(
      new HostRouterSpec,
      new ListingRouterSpec,
      new ReviewRouterSpec
    )
