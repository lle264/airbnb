package RouterSpecs

import RouterSpecs.ListingRouterSpec.{getRouter, instance}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import entity.HostEntity.HostCreatedResponse
import entity.ListingEntity
import entity.ListingEntity.{GetListingResponse, Listing}
import org.scalatest.Inside.inside
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.should.Matchers._
import org.scalatest.{BeforeAndAfterEach, GivenWhenThen}
import router._

import scala.concurrent.Await
import scala.language.postfixOps
import scala.util.Success

object ListingRouterSpec extends MyTestkitSetup with ListingFormats {
  def instance(): ListingInput = ListingInput(
    name = "Name",
    country = "Country",
    state = "State",
    city = "City",
    neighbourhoodCleansed = "Neighbourhood Cleansed",
    latitude = 0.0d,
    longitude = 0.0d,
    propertyType = "Property Type",
    roomType = "Room Type",
    accommodates = 5,
    bathrooms = 2.0d,
    bedrooms = 3,
    amenities = Set.empty,
    price = 500,
    minimumNights = 3,
    maximumNights = 10,
    availability365 = 128
  )
  def getRouter: ListingRouter = {
    val clusterShardingStub: ClusterShardingStub = getStub
    new ListingRouter(
      typedSystem,
      clusterShardingStub
    )
  }
}

class ListingRouterSpec
    extends AnyFeatureSpec
    with GivenWhenThen
    with ScalatestRouteTest
    with SprayJsonSupport
    with ListingFormats
    with BeforeAndAfterEach
    with MyTestkitSetup {

  var listingRouter: ListingRouter = _

  override def afterAll(): Unit = testKit.shutdownTestKit()

  Feature("Create a Listing") {
    Scenario("Creating a Listing") {
      Given("a new Listing")
      listingRouter = getRouter
      val l = instance()
      When("a create request is made")
      val r = Post("/listing/", l) ~> listingRouter.routesListing
      Then("a Listing should be created successfully")
      r ~> check {
        status shouldEqual StatusCodes.Created
        inside(responseAs[Listing]) { case x: Listing =>
          x.name shouldEqual l.name
          x.city shouldEqual l.city
        }
      }
    }
  }
  Feature("Read a Listing") {
    Scenario("Requested a nonexistent Listing") {
      Given("no Listing")
      listingRouter = getRouter
      When("a read request is made")
      val r = Get("/listing/1234") ~> listingRouter.routesListing
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested a deleted Listing") {
      Given("a deleted listing")
      listingRouter = getRouter
      val l = instance()
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      Await.result(listingRouter.deleteListing(id), timeout.duration)
      When(s"a read request is made for listing $id")
      val r = Get(f"/listing/$id") ~> listingRouter.routesListing
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Requested an existing Listing") {
      Given("an existing Listing")
      listingRouter = getRouter
      val l = instance()
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      When(s"a read request is made for listing $id")
      val r = Get(f"/listing/$id") ~> listingRouter.routesListing
      Then("that Listing should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
        inside(responseAs[Listing]) { case x: Listing =>
          x.name shouldEqual l.name
        }
      }
    }
  }

  Feature("Adding a Host to a Listing") {
    Scenario("Listing did not exist") {
      Given("no Listing, and an existing host")
      listingRouter = getRouter
      val h = Await
        .result(
          HostRouterSpec.getRouter.createHost(HostRouterSpec.instance()),
          timeout.duration
        )
        .asInstanceOf[HostCreatedResponse]
        .host
        .get
        .id
      val id = "1234"
      When("an Add Host request is made")
      val r = Post(
        s"/listing/?listingId=$id&hostId=$h"
      ) ~> listingRouter.routesListing
      Then("Bad Request should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Listing was deleted") {
      Given("a deleted listing, and a host")
      listingRouter = getRouter
      val l = instance()
      val h = Await
        .result(
          HostRouterSpec.getRouter.createHost(HostRouterSpec.instance()),
          timeout.duration
        )
        .asInstanceOf[HostCreatedResponse]
        .host
        .get
        .id
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      Await.result(listingRouter.deleteListing(id), timeout.duration)
      When(s"an Add Host request is made for listing $id")
      val r = Post(
        s"/listing/?listingId=$id&hostId=$h"
      ) ~> listingRouter.routesListing
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Host successfully added to Listing") {
      Given(
        "an existing, and a host"
      ) //TODO this actually needs a host to exist
      listingRouter = getRouter
      val l = instance()
      val h = Await
        .result(
          HostRouterSpec.getRouter.createHost(HostRouterSpec.instance()),
          timeout.duration
        )
        .asInstanceOf[HostCreatedResponse]
        .host
        .get
        .id
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      When(s"an Add Host request is made for listing $id")
      val r = Post(
        s"/listing/?listingId=$id&hostId=$h"
      ) ~> listingRouter.routesListing
      Then("OK should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
      }
      And("The listing should be updated")
      listingRouter.getListing(id).onComplete {
        case Success(GetListingResponse(Some(x: Listing))) =>
          inside(x) { case listing: ListingEntity.Listing =>
            listing.hosts should contain("HostID")
            listing.hosts should have size 1
          }
      }

    }
  }

  Feature("Update a Listing") {
    Scenario("Listing did not exist") {
      Given("no Listing, but an updated value for one")
      listingRouter = getRouter
      val n = instance().copy(name = "Updated")
      When("a read request is made")
      val r = Put("/listing/1234", n) ~> listingRouter.routesListing
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Listing was deleted") {
      Given("a deleted listing,an updated value for one")
      listingRouter = getRouter
      val l = instance()
      val n = instance().copy(name = "Updated")
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      Await.result(listingRouter.deleteListing(id), timeout.duration)
      When(s"an update request is made for listing $id")
      val r = Put(f"/listing/$id", n) ~> listingRouter.routesListing
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Listing successfully updated") {
      Given("an existing,an updated value for one")
      listingRouter = getRouter
      val l = instance()
      val n = instance().copy(name = "Updated")
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      When(s"an update request is made for listing $id")
      val r = Put(f"/listing/$id", n) ~> listingRouter.routesListing
      Then("OK should be returned")
      r ~> check {
        status shouldEqual StatusCodes.OK
      }
      And("The listing should be updated")
      listingRouter.getListing(id).onComplete {
        case Success(GetListingResponse(Some(x: Listing))) =>
          inside(x) { case listing: ListingEntity.Listing =>
            listing.name shouldEqual n.name
            listing.city shouldEqual l.city
          }
      }

    }
  }
  Feature("Delete a Listing") {
    Scenario("Listing did not exist") {
      Given("no Listing")
      listingRouter = getRouter
      When("a delete request is made")
      val r = Delete("/listing/1234") ~> listingRouter.routesListing
      Then("Not Found should  be returned")
      r ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }
    Scenario("Listing was deleted") {
      Given("a deleted listing")
      listingRouter = getRouter
      val l = instance()
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      Await.result(listingRouter.deleteListing(id), timeout.duration)
      When(s"a delete request is made for listing $id")
      val r = Delete(f"/listing/$id") ~> listingRouter.routesListing
      Then("Not Found should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    Scenario("Listing successfully deleted") {
      Given("a listing")
      listingRouter = getRouter
      val l = instance()
      val id = Await
        .result(listingRouter.createListing(l), timeout.duration)
        .asInstanceOf[ListingEntity.ListingCreatedResponse]
        .listing
        .get
        .id
      When(s"an update request is made for listing $id")
      val r = Delete(f"/listing/$id") ~> listingRouter.routesListing
      Then("NoContent should be returned")
      r ~> check {
        status shouldEqual StatusCodes.NoContent
      }
    }
  }
}
