import akka.actor.{ActorSystem, typed}
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.cluster.sharding.typed._
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.cluster.sharding.typed.testkit.scaladsl.TestEntityRef
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.persistence.typed.PersistenceId
import akka.testkit.TestDuration
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import entity._
import org.scalamock.scalatest.MockFactory

import scala.concurrent.duration.DurationInt

package object RouterSpecs extends MockFactory {

  trait ClusterShardingStub
      extends javadsl.ClusterSharding
      with scaladsl.ClusterSharding
  val testKit: ActorTestKit = ActorTestKit(
    "RouterSpec_TestKit",
    ConfigFactory.parseString("""
      akka.loglevel = INFO
      akka.actor.provider = cluster
      akka.remote.classic.netty.tcp.port = 2233
      akka.remote.artery.canonical.port = 2266
      akka.remote.artery.canonical.hostname = 127.0.0.1
      akka.persistence.journal.plugin = "akka.persistence.journal.inmem"
      akka.persistence.journal.inmem.test-serialization = on
      akka.actor.allow-java-serialization = on
    """)
  )

  trait MyTestkitSetup {
    val typedSystem: typed.ActorSystem[_] = testKit.internalSystem
    implicit val timeout: Timeout = 2.seconds
    implicit def default(implicit system: ActorSystem): RouteTestTimeout =
      RouteTestTimeout(
        new DurationInt(1).second.dilated(system)
      )

    def getStub: ClusterShardingStub = {
      val clusterShardingStub = stub[ClusterShardingStub]
      (clusterShardingStub
        .entityRefFor[ListingEntity.Command](
          _: EntityTypeKey[ListingEntity.Command],
          _: String
        ))
        .when(ListingEntity.TypeKey, *)
        .onCall((k: EntityTypeKey[ListingEntity.Command], id: String) =>
          TestEntityRef(
            k,
            id,
            testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
          )
        )
        .anyNumberOfTimes()
      (clusterShardingStub
        .entityRefFor[HostEntity.Command](
          _: EntityTypeKey[HostEntity.Command],
          _: String
        ))
        .when(HostEntity.TypeKey, *)
        .onCall((k: EntityTypeKey[HostEntity.Command], id: String) =>
          TestEntityRef(
            k,
            id,
            testKit.spawn(HostEntity(PersistenceId("Host", id)))
          )
        )
        .anyNumberOfTimes()
      (clusterShardingStub
        .entityRefFor[ReviewEntity.Command](
          _: EntityTypeKey[ReviewEntity.Command],
          _: String
        ))
        .when(ReviewEntity.TypeKey, *)
        .onCall((k: EntityTypeKey[ReviewEntity.Command], id: String) =>
          TestEntityRef(
            k,
            id,
            testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
          )
        )
        .anyNumberOfTimes()
      clusterShardingStub
    }
  }
}
