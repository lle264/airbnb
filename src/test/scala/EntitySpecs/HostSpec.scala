package EntitySpecs

import akka.Done
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.{ActorRef, Scheduler}
import akka.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.typed.PersistenceId
import entity.HostEntity
import entity.HostEntity._
import org.scalatest.Inside.inside
import org.scalatest._
import org.scalatest.featurespec.AnyFeatureSpecLike

import java.util.UUID
import scala.concurrent.Await
import scala.language.postfixOps
import scala.util.Success
class HostSpec
    extends ScalaTestWithActorTestKit(EventSourcedBehaviorTestKit.config)
    with AnyFeatureSpecLike
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with GivenWhenThen
    with TryValues {

  override def afterAll(): Unit = testKit.shutdownTestKit()

  val scheduler: Scheduler = schedulerFromActorSystem

  private val host = (id: String) =>
    HostEntity.Host(
      id = id,
      name = "Name",
      responseRate = 1,
      superHost = false
    )
  private val createMessage = (h: Host, replyTo: ActorRef[Response]) =>
    CreateHost(
      id = h.id,
      name = h.name,
      responseRate = h.responseRate,
      superHost = h.superHost,
      replyTo = replyTo
    )
  private val updateMessage = (h: Host, replyTo: ActorRef[Response]) =>
    UpdateHost(
      name = h.name,
      responseRate = h.responseRate,
      superHost = h.superHost,
      replyTo = replyTo
    )
  Feature("Create a Host") {
    Scenario("An existing Host is created") {
      Given("An existing Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val probe = testKit.createTestProbe[HostEntity.Response]()
      Await.result(
        a.ask[HostEntity.Response](createMessage(h, _)),
        atMost = timeout.duration
      )
      When("Host is Created")
      a ! createMessage(h, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case HostCreatedResponse(re) =>
        re.failure.exception should have message "Host already created"
      }
    }
    Scenario("A New Host is created") {
      Given("A new Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val probe = testKit.createTestProbe[HostEntity.Response]()
      When("Host is Created")
      a ! createMessage(h, probe.ref)
      Then("Host should be created")
      probe.expectMessage(HostCreatedResponse(Success(h)))
    }
  }
  Feature("As an user I can Read a Host") {
    Scenario("An un-initialized Host is requested") {
      Given("An un-initialized Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val probe = testKit.createTestProbe[HostEntity.Response]()
      When("Host is Read")
      a ! GetHost(
        id = id,
        replyTo = probe.ref
      )
      Then("Nothing should be returned")
      probe.expectMessage(GetHostResponse(None))
    }
    Scenario("An existing Host is requested") {
      Given("An existing Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val probe = testKit.createTestProbe[HostEntity.Response]()
      Await.result(
        a.ask[HostEntity.Response](createMessage(h, _)),
        atMost = timeout.duration
      )
      When("Host is Read")
      a ! GetHost(
        id = h.id,
        replyTo = probe.ref
      )
      Then("Host state should be received")
      probe.expectMessage(GetHostResponse(Some(h)))
    }
  }
  Feature("Update a Host") {
    Scenario("An un-initialized Host is updated") {
      Given("An un-initialized Host, and an updated value for it")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val n = h.copy(name = "Updated")
      val probe = testKit.createTestProbe[HostEntity.Response]()
      When("Host receives Update")
      a ! updateMessage(n, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case HostUpdatedResponse(re) =>
        re.failure.exception should have message "Host has not been created yet"
      }
    }
    Scenario("An existing Host is updated") {
      Given("An existing Host, and an updated value for it")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val n = h.copy(name = "Updated")
      val probe = testKit.createTestProbe[HostEntity.Response]()
      Await.result(
        a.ask[HostEntity.Response](createMessage(h, _)),
        atMost = timeout.duration
      )
      When("Host is Updated")
      a ! updateMessage(n, probe.ref)
      Then("Host state should be received")
      probe.expectMessage(HostUpdatedResponse(Success(Done)))
      a ! GetHost(id = n.id, replyTo = probe.ref)
      probe.expectMessage(GetHostResponse(Some(n)))
    }
  }
  Feature("Remove a Host") {
    Scenario("An un-initialized Host is removed") {
      Given("An un-initialized Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val probe = testKit.createTestProbe[HostEntity.Response]()
      When("Host is Removed")
      a ! RemoveHost(probe.ref)
      Then("Host should be Deleted")
      val r = probe.receiveMessage()
      inside(r) { case HostDeletedResponse(re) =>
        re.failure.exception should have message "Host doesn't exist"
      }
    }
    Scenario("An existing Host is removed") {
      Given("An existing Host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(HostEntity(PersistenceId("Host", id)))
      val h = host(id)
      val probe = testKit.createTestProbe[HostEntity.Response]()
      Await.result(
        a.ask[HostEntity.Response](createMessage(h, _)),
        atMost = timeout.duration
      )
      When("Host is Removed")
      a ! RemoveHost(probe.ref)
      Then("Host should be Deleted")
      probe.expectMessage(HostDeletedResponse(Success(Done)))
      a ! GetHost(id = h.id, replyTo = probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! createMessage(h, probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! updateMessage(h, probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! RemoveHost(probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
    }
  }

}
