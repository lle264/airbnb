package EntitySpecs

import akka.Done
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import akka.actor.typed.{ActorRef, Scheduler}
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.typed.PersistenceId
import entity.ListingEntity
import entity.ListingEntity._
import org.scalatest.Inside.inside
import org.scalatest._
import org.scalatest.featurespec.AnyFeatureSpecLike

import java.util.UUID
import scala.concurrent.Await
import scala.language.postfixOps
import scala.util.Success

class ListingSpec
    extends ScalaTestWithActorTestKit(EventSourcedBehaviorTestKit.config)
    with AnyFeatureSpecLike
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with GivenWhenThen
    with TryValues {

  override def afterAll(): Unit = testKit.shutdownTestKit()

  val scheduler: Scheduler = schedulerFromActorSystem
  private val listing = (id: String) =>
    ListingEntity.Listing(
      id = id,
      name = "Name",
      country = "Country",
      state = "State",
      city = "City",
      neighbourhoodCleansed = "Neighbourhood Cleansed",
      latitude = 0.0d,
      longitude = 0.0d,
      propertyType = "Property Type",
      roomType = "Room Type",
      accommodates = 5,
      bathrooms = 2.0d, //should they be floats?
      bedrooms = 3,
      amenities = Set.empty,
      price = 500,
      minimumNights = 3,
      maximumNights = 10,
      availability365 = 128,
      hosts = Set.empty
    )
  private val createMessage = (listing: Listing, replyTo: ActorRef[Response]) =>
    CreateListing(
      id = listing.id,
      name = listing.name,
      country = listing.country,
      state = listing.state,
      city = listing.city,
      neighbourhoodCleansed = listing.neighbourhoodCleansed,
      latitude = listing.latitude,
      longitude = listing.longitude,
      propertyType = listing.propertyType,
      roomType = listing.roomType,
      accommodates = listing.accommodates,
      bathrooms = listing.bathrooms,
      bedrooms = listing.bedrooms,
      amenities = listing.amenities,
      price = listing.price,
      minimumNights = listing.minimumNights,
      maximumNights = listing.maximumNights,
      availability365 = listing.availability365,
      replyTo = replyTo
    )
  private val updateMessage = (listing: Listing, replyTo: ActorRef[Response]) =>
    UpdateListing(
      name = "New " + listing.name,
      country = listing.country,
      state = listing.state,
      city = listing.city,
      neighbourhoodCleansed = listing.neighbourhoodCleansed,
      latitude = listing.latitude,
      longitude = listing.longitude,
      propertyType = listing.propertyType,
      roomType = listing.roomType,
      accommodates = listing.accommodates,
      bathrooms = listing.bathrooms,
      bedrooms = listing.bedrooms,
      amenities = listing.amenities,
      price = listing.price,
      minimumNights = listing.minimumNights,
      maximumNights = listing.maximumNights,
      availability365 = listing.availability365,
      replyTo = replyTo
    )
  Feature("Create a Listing") {
    Scenario("An existing Listing is created") {
      Given("An existing Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      Await.result(
        a.ask[ListingEntity.Response](createMessage(list, _)),
        atMost = timeout.duration
      )
      When("Listing is Created")
      a ! createMessage(list, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case ListingCreatedResponse(re) =>
        re.failure.exception should have message "Listing already created"
      }
    }
    Scenario("A New Listing is created") {
      Given("A new Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      When("Listing is Created")
      a ! createMessage(list, probe.ref)
      Then("Listing should be created")
      probe.expectMessage(ListingCreatedResponse(Success(list)))
    }
  }
  Feature("As an user I can Read a Listing") {
    Scenario("An un-initialized Listing is requested") {
      Given("An un-initialized Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      When("Listing is Read")
      a ! GetListing(
        id = id,
        replyTo = probe.ref
      )
      Then("Nothing should be returned")
      probe.expectMessage(GetListingResponse(None))
    }
    Scenario("An existing Listing is requested") {
      Given("An existing Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      Await.result(
        a.ask[ListingEntity.Response](createMessage(list, _)),
        atMost = timeout.duration
      )
      When("Listing is Read")
      a ! GetListing(
        id = list.id,
        replyTo = probe.ref
      )
      Then("Listing state should be received")
      probe.expectMessage(GetListingResponse(Some(list)))
    }
  }
  Feature("Update a Listing") {
    Scenario("An un-initialized Listing is updated") {
      Given("An un-initialized Listing, and an updated one")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      When("Listing receives Update")
      a ! updateMessage(list, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case ListingUpdatedResponse(re) =>
        re.failure.exception should have message "Listing has not been created yet"
      }
    }
    Scenario("An existing Listing is updated") {
      Given("An existing Listing, and an updated one")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      Await.result(
        a.ask[ListingEntity.Response](createMessage(list, _)),
        atMost = timeout.duration
      )
      When("Listing is Updated")
      a ! updateMessage(list, probe.ref)
      Then("Listing state should be received")
      probe.expectMessage(ListingUpdatedResponse(Success(Done)))
      a ! GetListing(id = list.id, replyTo = probe.ref)
      probe.receiveMessage() match {
        case GetListingResponse(Some(review)) =>
          review.name shouldBe "New " + list.name
      }
    }
  }
  Feature("Add a Host to a Listing") {
    Scenario("An un-initialized Listing receives a new Host") {
      Given("An un-initialized Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      When("Listing receives a new host")
      a ! AddHostToListing(id, "host", probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case HostAddedToListingResponse(re) =>
        re.failure.exception should have message "Listing doesn't exist"
      }
    }
    Scenario("A Listing receives a new Host") {
      Given("An existing Listing, a new host")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val host = "Host"
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      Await.result(
        a.ask[ListingEntity.Response](createMessage(list, _)),
        atMost = timeout.duration
      )
      When("Listing receives a new host")
      a ! AddHostToListing(id, host, probe.ref)
      Then("Host should be added to listing")
      probe.expectMessage(
        HostAddedToListingResponse(Success(list.copy(hosts = Set(host))))
      )
    }
  }
  Feature("Remove a Listing") {
    Scenario("An un-initialized Listing is removed") {
      Given("An un-initialized Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      When("Listing is Removed")
      a ! RemoveListing(probe.ref)
      Then("Listing should be Deleted")
      val r = probe.receiveMessage()
      inside(r) { case ListingDeletedResponse(re) =>
        re.failure.exception should have message "Listing doesn't exist"
      }

    }
    Scenario("An existing Listing is removed") {
      Given("An existing Listing")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ListingEntity(PersistenceId("Listing", id)))
      val list = listing(id)
      val probe = testKit.createTestProbe[ListingEntity.Response]()
      Await.result(
        a.ask[ListingEntity.Response](createMessage(list, _)),
        atMost = timeout.duration
      )
      When("Listing is Removed")
      a ! RemoveListing(probe.ref)
      probe.expectMessage(ListingDeletedResponse(Success(Done)))
      Then("Listing should be Deleted")
      a ! GetListing(id = list.id, replyTo = probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! createMessage(list, probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! updateMessage(list, probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! RemoveListing(probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
    }
  }

}
