package EntitySpecs

import akka.Done
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import akka.actor.typed.{ActorRef, Scheduler}
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.typed.PersistenceId
import entity.ReviewEntity
import entity.ReviewEntity._
import org.scalatest.Inside.inside
import org.scalatest._
import org.scalatest.featurespec.AnyFeatureSpecLike

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.Await
import scala.language.postfixOps
import scala.util.Success

class ReviewSpec
    extends ScalaTestWithActorTestKit(EventSourcedBehaviorTestKit.config)
    with AnyFeatureSpecLike
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with GivenWhenThen
    with TryValues {

  override def afterAll(): Unit = testKit.shutdownTestKit()

  val scheduler: Scheduler = schedulerFromActorSystem
  private val review = (id: String) =>
    ReviewEntity.Review(
      id = id,
      listingId = "",
      reviewerId = "reviewerID",
      reviewerName = "Reviewer",
      comment = "Comment",
      date = LocalDate.now()
    )
  private val addMessage = (review: Review, replyTo: ActorRef[Response]) =>
    AddReview(
      id = review.id,
      listingId = review.listingId,
      reviewerId = review.reviewerId,
      reviewerName = review.reviewerName,
      comment = review.comment,
      replyTo = replyTo
    )
  Feature("Create a Review") {
    Scenario("An existing Review is created") {
      Given("An existing Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val rev = review(id)
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      Await.result(
        a.ask[ReviewEntity.Response](addMessage(rev, _)),
        atMost = timeout.duration
      )
      When("Review is Created")
      a ! addMessage(rev, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case ReviewAddedResponse(re) =>
        re.failure.exception should have message "Review already added"
      }
    }
    Scenario("A New Review is created") {
      Given("A new Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val rev = review(id)
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      When("Review is Created")
      a ! addMessage(rev, probe.ref)
      Then("Review should be created")
      probe.expectMessage(ReviewAddedResponse(Success(rev)))
    }
  }
  Feature("As an user I can Read a Review") {
    Scenario("An un-initialized Review is requested") {
      Given("An un-initialized Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      When("Review is Read")
      a ! GetReview(
        id = id,
        replyTo = probe.ref
      )
      Then("Nothing should be returned")
      probe.expectMessage(GetReviewResponse(None))
    }
    Scenario("An existing Review is requested") {
      Given("An existing Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val rev = review(id)
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      Await.result(
        a.ask[ReviewEntity.Response](addMessage(rev, _)),
        atMost = timeout.duration
      )
      When("Review is Read")
      a ! GetReview(
        id = rev.id,
        replyTo = probe.ref
      )
      Then("Review state should be received")
      probe.expectMessage(GetReviewResponse(Some(rev)))
    }
  }
  Feature("Update a Review's comment") {
    Scenario("An un-initialized Review is updated") {
      Given("An un-initialized Review, and an updated comment")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val c = "Different Comment"
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      When("Review receives Update")
      a ! UpdateReview(c, probe.ref)
      Then("Exception should be returned")
      val r = probe.receiveMessage()
      inside(r) { case ReviewUpdatedResponse(re) =>
        re.failure.exception should have message "Review has not been created yet"
      }
    }
    Scenario("An existing Review is updated") {
      Given("An existing Review, and an updated comment")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val c = "Different Comment"
      val rev = review(id)
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      Await.result(
        a.ask[ReviewEntity.Response](addMessage(rev, _)),
        atMost = timeout.duration
      )
      When("Review is Updated")
      a ! UpdateReview(c, probe.ref)
      Then("Review state should be received")
      probe.expectMessage(ReviewUpdatedResponse(Success(Done)))
      a ! GetReview(id = rev.id, replyTo = probe.ref)
      probe.receiveMessage() match {
        case GetReviewResponse(Some(review)) => review.comment shouldBe c
      }
    }
  }
  Feature("Remove a Review") {
    Scenario("An un-initialized Review is removed") {
      Given("An un-initialized Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      When("Review is Removed")
      a ! RemoveReview(probe.ref)
      Then("Review should be Deleted") //TODO check this
      val r = probe.receiveMessage()
      inside(r) { case ReviewDeletedResponse(re) =>
        re.failure.exception should have message "Review doesn't exist"
      }

    }
    Scenario("An existing Review is removed") {
      Given("An existing Review")
      val id = UUID.randomUUID().toString
      val a = testKit.spawn(ReviewEntity(PersistenceId("Review", id)))
      val rev = review(id)
      val probe = testKit.createTestProbe[ReviewEntity.Response]()
      Await.result(
        a.ask[ReviewEntity.Response](addMessage(rev, _)),
        atMost = timeout.duration
      )
      When("Review is Removed")
      a ! RemoveReview(probe.ref)
      Then("Review should be Deleted")
      probe.expectMessage(ReviewDeletedResponse(Success(Done)))
      a ! GetReview(id = rev.id, replyTo = probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! addMessage(rev, probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! UpdateReview("", probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
      a ! RemoveReview(probe.ref)
      probe.expectMessage(AlreadyDeletedResponse)
    }
  }

}
