package app

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}
import entity.{HostEntity, ListingEntity, ReviewEntity}
import akka.actor.typed.{ActorSystem, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.server.Directives.{concat, pathPrefix}
import akka.http.scaladsl.{Http, server}
import router.{HostRouter, ListingRouter, LoaderRouter, ReviewRouter}

object AirbnbApp {

  def startHttpServer()(implicit system: ActorSystem[_]): Unit = {
    val routerListing = new ListingRouter(system, ClusterSharding(system))
    val routerHost = new HostRouter(system, ClusterSharding(system))
    val routerReview = new ReviewRouter(system, ClusterSharding(system))
    val routerLoaderListing = new LoaderRouter(system, ClusterSharding(system))

    val routes =
      pathPrefix("airbnb") {
        concat(
          routerListing.routesListing,
          routerHost.routesHost,
          routerReview.routesReview,
          routerLoaderListing.routesLoader
        )
      }

    createHttpServer("127.0.0.1", 8080, routes)
  }

  def createHttpServer(host: String, port: Int, routes: server.Route)(implicit
      system: ActorSystem[_]
  ): Unit = {
    implicit val ec: ExecutionContext = system.executionContext

    val httpBindingFuture = Http().newServerAt(host, port).bind(routes)
    httpBindingFuture.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info(
          s"Server online at http://${address.getHostString}:${address.getPort}"
        )
      case Failure(exception) =>
        system.log.error(s"Failed to bind HTTP server, because: $exception")
    }
  }

  def main(args: Array[String]): Unit = {
    sealed trait RootCommand

    val rootBehavior: Behavior[RootCommand] = Behaviors.setup { context =>
      ListingEntity.initSharding(context.system)
      HostEntity.initSharding(context.system)
      ReviewEntity.initSharding(context.system)

      startHttpServer()(context.system)

      Behaviors.unhandled
    }

    ActorSystem(rootBehavior, "AirbnbSystem")

  }

}
