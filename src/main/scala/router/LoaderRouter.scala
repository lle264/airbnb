package router

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.model.Multipart
import akka.http.scaladsl.model.Multipart.FormData.BodyPart
import akka.{Done, NotUsed}
import entity.ListingEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Framing, Sink}
import akka.util.{ByteString, Timeout}

class LoaderRouter(system: ActorSystem[_], sharding: ClusterSharding) {
  implicit val mat: Materializer = Materializer(system)

  val splitLines: Flow[ByteString, ByteString, NotUsed] =
    Framing.delimiter(
      ByteString(System.lineSeparator()),
      10000,
      allowTruncation = true
    )

  private def saveListing(
      input: Vector[String]
  ): Future[ListingEntity.Response] = {
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, input(0))
    implicit val timeout: Timeout = Timeout(60.seconds)

    ref.ask { replyTo =>
      ListingEntity.CreateListing(
        input(0),
        input(1),
        input(11),
        input(10),
        input(8),
        input(9),
        stringToDouble(input(12)),
        stringToDouble(input(13)),
        input(14),
        input(15),
        stringToInt(input(16)),
        stringToDouble(input(17)),
        stringToInt(input(18)),
        input(19).split(";").toSet,
        stringToInt(input(20)),
        stringToInt(input(21)),
        stringToInt(input(22)),
        stringToInt(input(23)),
        replyTo
      )
    }
  }

  private def stringToDouble(s: String, default: Double = 0): Double =
    s.toDoubleOption match {
      case Some(value) => value
      case None        => default
    }

  private def stringToInt(s: String, default: Int = 0): Int =
    s.toIntOption match {
      case Some(value) => value
      case None        => default
    }

  val routesLoader: Route = pathPrefix("load") {
    concat(
      path("listing") {
        pathEndOrSingleSlash {
          withSizeLimit(3216764835L) {
            entity(as[Multipart.FormData]) { formData =>
              val done: Future[Done] = formData.parts
                .mapAsync(8) {
                  case b: BodyPart if b.filename.exists(_.endsWith(".csv")) =>
                    b.entity.dataBytes
                      .via(splitLines)
                      .map(_.utf8String.split(",").map(_.trim).toVector)
                      .mapAsyncUnordered(4) { record =>
                        saveListing(record).recover {
                          case e: akka.pattern.AskTimeoutException =>
                            Future(
                              ListingEntity.ListingCreatedResponse(Failure(e))
                            )(system.executionContext)
                        }(system.executionContext)
                      }
                      .runForeach {
                        case ListingEntity
                              .ListingCreatedResponse(Success(value)) =>
                          system.log.info(value.name)
                        case ListingEntity
                              .ListingCreatedResponse(Failure(exception)) =>
                          system.log.error(s"$exception")
                      }
                  case _ => Future.successful(Done)
                }
                .runWith(Sink.ignore)
              onSuccess(done) { _ =>
                system.log.info("Finish loading of listings data")
                complete("Data loading completed!")
              }
            }
          }
        }
      },
      path("review") {
        complete("review load ok")
      }
    )
  }

}
