package router

import java.util.UUID
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import entity.HostEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

case class HostInput(name: String, responseRate: Double, superHost: Boolean)
trait HostFormats {

  implicit val hostInputFormat: RootJsonFormat[HostInput] = jsonFormat3(
    HostInput
  )
  implicit val hostEntity: RootJsonFormat[HostEntity.Host] = jsonFormat4(
    HostEntity.Host
  )
}

class HostRouter(system: ActorSystem[_], sharding: ClusterSharding)
    extends HostFormats {

  implicit val timeout: Timeout = Timeout(5.seconds)

  def createHost(hostInput: HostInput): Future[HostEntity.Response] = {
    val id = UUID.randomUUID().toString
    val ref = sharding.entityRefFor(HostEntity.TypeKey, id)
    system.log.info(s"Received message for create a host with id: $id")
    ref.ask(replyTo =>
      HostEntity.CreateHost(
        id,
        hostInput.name,
        hostInput.responseRate,
        hostInput.superHost,
        replyTo
      )
    )
  }
  def deleteHost(id: String): Future[HostEntity.Response] = {
    val ref = sharding.entityRefFor(HostEntity.TypeKey, id)
    ref ask (replyTo => HostEntity.RemoveHost(replyTo))
  }
  def updateHost(
      id: String,
      hostInput: HostInput
  ): Future[HostEntity.Response] = {
    val ref = sharding.entityRefFor(HostEntity.TypeKey, id)
    system.log.info(s"Received message for update a host with id: $id")
    ref.ask(replyTo =>
      HostEntity.UpdateHost(
        hostInput.name,
        hostInput.responseRate,
        hostInput.superHost,
        replyTo
      )
    )
  }

  def getHost(id: String): Future[HostEntity.Response] = {
    val ref = sharding.entityRefFor(HostEntity.TypeKey, id)
    ref.ask(replyTo => HostEntity.GetHost(id, replyTo))
  }

  val routesHost: Route = pathPrefix("host") {
    concat(
      path(Segment) { id =>
        get {
          onSuccess(getHost(id)) {
            case HostEntity.GetHostResponse(Some(host)) =>
              complete(StatusCodes.OK, host)
            case HostEntity.GetHostResponse(None) =>
              complete(StatusCodes.NotFound)
            case HostEntity.AlreadyDeletedResponse =>
              complete(StatusCodes.NotFound)
            case _ => complete(StatusCodes.InternalServerError)
          }
        } ~
          put {
            entity(as[HostInput]) { host =>
              onSuccess(updateHost(id, host)) {
                case HostEntity.HostUpdatedResponse(Success(Done)) =>
                  complete(StatusCodes.OK)
                case HostEntity.HostUpdatedResponse(Failure(exception)) =>
                  complete(StatusCodes.BadRequest, s"${exception.getMessage}")
                case HostEntity.AlreadyDeletedResponse =>
                  complete(StatusCodes.NotFound)
                case _ => complete(StatusCodes.InternalServerError)
              }
            }
          } ~
          delete {
            onSuccess(deleteHost(id)) {
              case HostEntity.HostDeletedResponse(Success(Done)) =>
                complete(StatusCodes.NoContent)
              case HostEntity.HostDeletedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, exception.getMessage)
              case HostEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound)
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
      },
      pathEndOrSingleSlash {
        post {
          entity(as[HostInput]) { host =>
            onSuccess(createHost(host)) {
              case HostEntity.HostCreatedResponse(Success(host)) =>
                complete(StatusCodes.Created, host)
              case HostEntity.HostCreatedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, s"${exception.getMessage}")
              case HostEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound) //unreachable?
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
        }
      }
    )
  }

}
