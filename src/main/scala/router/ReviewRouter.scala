package router

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import entity.{ListingEntity, ReviewEntity}
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._
import spray.json._

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util._

case class ReviewInput(
    listingId: String,
    reviewerId: String,
    reviewerName: String,
    comment: String
)
case class Comment(comment: String)

trait ReviewFormats {
  implicit val commentFormat: RootJsonFormat[Comment] = jsonFormat1(Comment)
  implicit val failureResponseFormat: RootJsonFormat[FailureResponse] =
    jsonFormat1(FailureResponse)

  implicit val reviewInputFormat: RootJsonFormat[ReviewInput] = jsonFormat4(
    ReviewInput
  )
  implicit val dateFormat: JsonFormat[LocalDate] = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue = JsString(obj.toString)

    override def read(json: JsValue): LocalDate = json match {
      case JsString(s) =>
        Try(LocalDate.parse(s)) match {
          case Success(result) => result
          case Failure(exception) =>
            deserializationError(s"could not parse $s as LocalDate", exception)
        }
      case _ =>
        deserializationError(s"Something went wrong with the deserialization")
    }
  }
  implicit val reviewFormat: RootJsonFormat[ReviewEntity.Review] = jsonFormat6(
    ReviewEntity.Review
  )
}

class ReviewRouter(system: ActorSystem[_], sharding: ClusterSharding)
    extends ReviewFormats {

  implicit val timeout: Timeout = Timeout(5.seconds)

  def createReview(
      reviewInput: ReviewInput
  ): Future[ReviewEntity.Response] = {
    val ref =
      sharding.entityRefFor(ListingEntity.TypeKey, reviewInput.listingId)
    val listing = ref.ask(replyTo =>
      ListingEntity.GetListing(reviewInput.listingId, replyTo)
    )
    implicit val actorSystem: ExecutionContextExecutor = system.executionContext
    listing.flatMap {
      case ListingEntity.GetListingResponse(Some(_)) =>
        val id = UUID.randomUUID().toString
        val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
        ref.ask { replyTo =>
          ReviewEntity.AddReview(
            id,
            reviewInput.listingId,
            reviewInput.reviewerId,
            reviewInput.reviewerName,
            reviewInput.comment,
            replyTo
          )
        }
      case ListingEntity.GetListingResponse(None) =>
        Future(
          ReviewEntity.ReviewAddedResponse(
            Failure(new RuntimeException("Listing can not be found"))
          )
        )
      case ListingEntity.AlreadyDeletedResponse =>
        Future(
          ReviewEntity.ReviewAddedResponse(
            Failure(new RuntimeException("Listing is no longer available"))
          )
        )
      case _ =>
        Future(
          ReviewEntity.ReviewAddedResponse(
            Failure(new RuntimeException("Something went wrong"))
          )
        )
    }
  }

  def updateReview(
      id: String,
      comment: Comment
  ): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
    ref.ask(replyTo => ReviewEntity.UpdateReview(comment.comment, replyTo))
  }

  def getReview(id: String): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
    ref.ask(replyTo => ReviewEntity.GetReview(id, replyTo))
  }
  def deleteReview(id: String): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
    ref ask (replyTo => ReviewEntity.RemoveReview(replyTo))
  }
  val routesReview: Route = pathPrefix("review") {
    concat(
      path(Segment) { id =>
        get {
          onSuccess(getReview(id)) {
            case ReviewEntity.GetReviewResponse(Some(review)) =>
              complete(StatusCodes.OK, review)
            case ReviewEntity.GetReviewResponse(None) =>
              complete(StatusCodes.NotFound)
            case ReviewEntity.AlreadyDeletedResponse =>
              complete(StatusCodes.NotFound)
            case _ => complete(StatusCodes.InternalServerError)
          }
        } ~
          put {
            entity(as[Comment]) { comment =>
              onSuccess(updateReview(id, comment)) {
                case ReviewEntity.ReviewUpdatedResponse(Success(Done)) =>
                  complete(StatusCodes.OK)
                case ReviewEntity.ReviewUpdatedResponse(Failure(exception)) =>
                  complete(
                    StatusCodes.BadRequest,
                    FailureResponse(s"${exception.getMessage}")
                  )
                case ReviewEntity.AlreadyDeletedResponse =>
                  complete(StatusCodes.NotFound)
                case _ => complete(StatusCodes.InternalServerError)
              }
            }
          } ~
          delete {
            onSuccess(deleteReview(id)) {
              case ReviewEntity.ReviewDeletedResponse(Success(Done)) =>
                complete(StatusCodes.NoContent)
              case ReviewEntity.ReviewDeletedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, exception.getMessage)
              case ReviewEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound)
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
      },
      pathEndOrSingleSlash {
        post {
          entity(as[ReviewInput]) { review =>
            onSuccess(createReview(review)) {
              case ReviewEntity.ReviewAddedResponse(Success(rev)) =>
                complete(
                  StatusCodes.Created,
                  rev
                )
              case ReviewEntity.ReviewAddedResponse(Failure(exception)) =>
                complete(
                  StatusCodes.BadRequest,
                  FailureResponse(s"${exception.getMessage}")
                )
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
        }
      }
    )
  }

}
