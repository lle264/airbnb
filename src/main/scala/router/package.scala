package object router {

  case class FailureResponse(reason: String)
}
