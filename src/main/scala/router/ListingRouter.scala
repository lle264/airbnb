package router

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import entity.{HostEntity, ListingEntity}
import akka.http.scaladsl.server.Directives.{
  Segment,
  as,
  complete,
  concat,
  delete,
  entity,
  get,
  onSuccess,
  parameters,
  path,
  pathEndOrSingleSlash,
  pathPrefix,
  post,
  put
}
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

case class ListingInput(
    name: String,
    country: String,
    state: String,
    city: String,
    neighbourhoodCleansed: String,
    latitude: Double,
    longitude: Double,
    propertyType: String,
    roomType: String,
    accommodates: Int,
    bathrooms: Double,
    bedrooms: Int,
    amenities: Set[String] = Set.empty,
    price: Int,
    minimumNights: Int,
    maximumNights: Int,
    availability365: Int
)

trait ListingFormats {
  implicit val listingInputFormat: RootJsonFormat[ListingInput] = jsonFormat17(
    ListingInput
  )
  implicit val listingEntityFormat: RootJsonFormat[ListingEntity.Listing] =
    jsonFormat19(ListingEntity.Listing)
  implicit val failureResponseFormat: RootJsonFormat[FailureResponse] =
    jsonFormat1(FailureResponse)
}
class ListingRouter(system: ActorSystem[_], val sharding: ClusterSharding)
    extends ListingFormats {
  implicit val timeout: Timeout = Timeout(5.seconds)

  def createListing(
      listingInput: ListingInput
  ): Future[ListingEntity.Response] = {
    val id = UUID.randomUUID().toString
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, id)
    system.log.info(s"Received message for create a listing with id: $id")
    ref.ask { replyTo =>
      ListingEntity.CreateListing(
        id,
        listingInput.name,
        listingInput.country,
        listingInput.state,
        listingInput.city,
        listingInput.neighbourhoodCleansed,
        listingInput.latitude,
        listingInput.longitude,
        listingInput.propertyType,
        listingInput.roomType,
        listingInput.accommodates,
        listingInput.bathrooms,
        listingInput.bedrooms,
        listingInput.amenities,
        listingInput.price,
        listingInput.minimumNights,
        listingInput.maximumNights,
        listingInput.availability365,
        replyTo
      )
    }
  }

  def updateListing(
      id: String,
      listingInput: ListingInput
  ): Future[ListingEntity.Response] = {
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, id)
    system.log.info(s"Received message for update a listing with id: $id")
    ref.ask { replyTo =>
      ListingEntity.UpdateListing(
        listingInput.name,
        listingInput.country,
        listingInput.state,
        listingInput.city,
        listingInput.neighbourhoodCleansed,
        listingInput.latitude,
        listingInput.longitude,
        listingInput.propertyType,
        listingInput.roomType,
        listingInput.accommodates,
        listingInput.bathrooms,
        listingInput.bedrooms,
        listingInput.amenities,
        listingInput.price,
        listingInput.minimumNights,
        listingInput.maximumNights,
        listingInput.availability365,
        replyTo
      )
    }
  }

  def getListing(id: String): Future[ListingEntity.Response] = {
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, id)
    ref.ask(replyTo => ListingEntity.GetListing(id, replyTo))
  }

  def addHostToListing(
      listingId: String,
      hostId: String
  ): Future[ListingEntity.Response] = {
    val ref = sharding.entityRefFor(HostEntity.TypeKey, hostId)
    val host = ref.ask(replyTo => HostEntity.GetHost(hostId, replyTo))
    implicit val actorSystem: ExecutionContextExecutor = system.executionContext
    host.flatMap {
      case HostEntity.GetHostResponse(Some(_)) =>
        val ref = sharding.entityRefFor(ListingEntity.TypeKey, listingId)
        ref.ask(replyTo =>
          ListingEntity.AddHostToListing(listingId, hostId, replyTo)
        )
      case HostEntity.GetHostResponse(None) =>
        Future(
          ListingEntity.HostAddedToListingResponse(
            Failure(new RuntimeException("Host can not be found"))
          )
        )
      case HostEntity.AlreadyDeletedResponse =>
        Future(
          ListingEntity.HostAddedToListingResponse(
            Failure(new RuntimeException("Host can not be found"))
          )
        )
      case _ =>
        Future(
          ListingEntity.HostAddedToListingResponse(
            Failure(new RuntimeException("Something went wrong"))
          )
        )
    }
  }

  def deleteListing(id: String): Future[ListingEntity.Response] = {
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, id)
    ref ask (replyTo => ListingEntity.RemoveListing(replyTo))
  }

  val routesListing: Route = pathPrefix("listing") {
    concat(
      path(Segment) { id =>
        concat(
          get {
            onSuccess(getListing(id)) {
              case ListingEntity.GetListingResponse(Some(listing)) =>
                complete(StatusCodes.OK, listing)
              case ListingEntity.GetListingResponse(None) =>
                complete(StatusCodes.NotFound)
              case ListingEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound)
              case _ => complete(StatusCodes.InternalServerError)
            }
          },
          put {
            entity(as[ListingInput]) { listing =>
              onSuccess(updateListing(id, listing)) {
                case ListingEntity.ListingUpdatedResponse(Success(Done)) =>
                  complete(StatusCodes.OK)
                case ListingEntity.ListingUpdatedResponse(Failure(exception)) =>
                  complete(StatusCodes.BadRequest, s"${exception.getMessage}")
                case ListingEntity.AlreadyDeletedResponse =>
                  complete(StatusCodes.NotFound)
                case _ => complete(StatusCodes.InternalServerError)
              }
            }
          },
          delete {
            onSuccess(deleteListing(id)) {
              case ListingEntity.ListingDeletedResponse(Success(Done)) =>
                complete(StatusCodes.NoContent)
              case ListingEntity.ListingDeletedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, exception.getMessage)
              case ListingEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound)
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
        )
      },
      pathEndOrSingleSlash {
        post {
          concat(
            entity(as[ListingInput]) { listing =>
              onSuccess(createListing(listing)) {
                case ListingEntity.ListingCreatedResponse(Success(listing)) =>
                  complete(StatusCodes.Created, listing)
                case ListingEntity.ListingCreatedResponse(Failure(exception)) =>
                  complete(StatusCodes.BadRequest, s"${exception.getMessage}")
                case _ => complete(StatusCodes.InternalServerError)
              }
            },
            parameters("listingId", "hostId") { (listingId, hostId) =>
              onSuccess(addHostToListing(listingId, hostId)) {
                case ListingEntity.HostAddedToListingResponse(
                      Success(listing)
                    ) =>
                  complete(StatusCodes.OK, listing)
                case ListingEntity.HostAddedToListingResponse(
                      Failure(exception)
                    ) =>
                  complete(
                    StatusCodes.BadRequest,
                    FailureResponse(s"${exception.getMessage}")
                  )
                case ListingEntity.AlreadyDeletedResponse =>
                  complete(StatusCodes.NotFound)
                case _ => complete(StatusCodes.InternalServerError)
              }
            }
          )
        }
      }
    )
  }

}
