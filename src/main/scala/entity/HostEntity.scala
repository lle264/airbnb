package entity

import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

import akka.Done
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{
  ClusterSharding,
  Entity,
  EntityTypeKey
}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{
  Effect,
  EventSourcedBehavior,
  ReplyEffect,
  RetentionCriteria
}

object HostEntity {

  val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("Host")

  def initSharding(system: ActorSystem[_]): Unit =
    ClusterSharding(system).init(Entity(TypeKey) { entityContext =>
      HostEntity(
        PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
      )
    })

  sealed trait State extends CborSerializable {
    def applyEvent(event: Event): State
  }
  final case object EmptyHost extends State {
    override def applyEvent(event: Event): State = event match {
      case HostCreated(host) => host
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [EmptyHost]"
        )
    }
  }
  final case class Host(
      id: String,
      name: String,
      responseRate: Double,
      superHost: Boolean
  ) extends State {
    override def applyEvent(event: Event): State = event match {
      case HostUpdated(host) => host
      case HostRemoved       => Removed
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [CreatedHost]"
        )
    }
  }
  final case object Removed extends State {
    override def applyEvent(event: Event): State = event match {
      case _ =>
        throw new IllegalStateException(
          "Not persist events allowed for a removed Host"
        )
    }
  }

  sealed trait Command
  case class CreateHost(
      id: String,
      name: String,
      responseRate: Double,
      superHost: Boolean,
      replyTo: ActorRef[Response]
  ) extends Command
  case class UpdateHost(
      name: String,
      responseRate: Double,
      superHost: Boolean,
      replyTo: ActorRef[Response]
  ) extends Command
  case class GetHost(id: String, replyTo: ActorRef[Response]) extends Command
  case class RemoveHost(replyTo: ActorRef[Response]) extends Command

  sealed trait Event extends CborSerializable
  case class HostCreated(host: Host) extends Event
  case class HostUpdated(host: Host) extends Event
  case object HostRemoved extends Event

  sealed trait Response
  case class HostCreatedResponse(host: Try[Host]) extends Response
  case class HostUpdatedResponse(host: Try[Done]) extends Response
  case class GetHostResponse(host: Option[Host]) extends Response
  case class HostDeletedResponse(host: Try[Done]) extends Response
  case object AlreadyDeletedResponse extends Response

  private def handleCommand(
      context: ActorContext[Command]
  ): (State, Command) => ReplyEffect[Event, State] = (state, command) =>
    state match {
      case EmptyHost =>
        command match {
          case cmd: CreateHost =>
            val host: Host =
              Host(cmd.id, cmd.name, cmd.responseRate, cmd.superHost)
            Effect
              .persist(HostCreated(host))
              .thenReply(cmd.replyTo) { _ =>
                context.log.info(s"Host created with the id: ${cmd.id}")
                HostCreatedResponse(Success(host))
              }
          case cmd: UpdateHost =>
            Effect.reply(cmd.replyTo) {
              HostUpdatedResponse(
                Failure(new RuntimeException("Host has not been created yet"))
              )
            }
          case GetHost(_, replyTo) =>
            Effect.reply(replyTo)(GetHostResponse(None))
          case RemoveHost(replyTo) =>
            Effect.reply(replyTo)(
              HostDeletedResponse(
                Failure((new RuntimeException("Host doesn't exist")))
              )
            )
        }
      case state: Host =>
        command match {
          case GetHost(_, replyTo) =>
            Effect.reply(replyTo)(GetHostResponse(Some(state)))
          case cmd: CreateHost =>
            Effect.reply(cmd.replyTo)(
              HostCreatedResponse(
                Failure(new RuntimeException("Host already created"))
              )
            )
          case cmd: UpdateHost =>
            val host = Host(state.id, cmd.name, cmd.responseRate, cmd.superHost)
            Effect.persist(HostUpdated(host)).thenReply(cmd.replyTo) { _ =>
              context.log.info(s"Host with id: ${host.id} updated")
              HostUpdatedResponse(Success(Done))
            }
          case cmd: RemoveHost =>
            context.log.info(s"Host with id: ${state.id} has been removed")
            Effect.persist(HostRemoved).thenReply(cmd.replyTo) { _ =>
              HostDeletedResponse(Success(Done))
            }
        }
      case Removed =>
        command match {
          case cmd: GetHost => Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: CreateHost =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: UpdateHost =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: RemoveHost =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
        }
    }

  private def handleEvent: (State, Event) => State = (state, event) =>
    state.applyEvent(event)

  def apply(persistenceId: PersistenceId): Behavior[Command] = Behaviors.setup {
    context =>
      EventSourcedBehavior
        .withEnforcedReplies[Command, Event, State](
          persistenceId = persistenceId,
          emptyState = EmptyHost,
          commandHandler = handleCommand(context),
          eventHandler = handleEvent
        )
        .withRetention(RetentionCriteria.snapshotEvery(100, 3))
        .onPersistFailure(
          SupervisorStrategy.restartWithBackoff(1000.millis, 60.second, 0.1)
        )
  }

}
