package entity

import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

import akka.Done
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{
  ClusterSharding,
  Entity,
  EntityTypeKey
}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{
  Effect,
  EventSourcedBehavior,
  ReplyEffect,
  RetentionCriteria
}

object ListingEntity {

  val TypeKey: EntityTypeKey[Command] =
    EntityTypeKey[Command]("Listing")

  def initSharding(system: ActorSystem[_]): Unit =
    ClusterSharding(system).init(Entity(TypeKey) { entityContext =>
      ListingEntity(
        PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
      )
    })

  sealed trait State extends CborSerializable {
    def applyEvent(event: Event): State
  }
  final case object EmptyListing extends State {
    override def applyEvent(event: Event): State = event match {
      case ListingCreated(listingEntity) => listingEntity
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [EmptyListing]"
        )
    }
  }
  final case class Listing(
      id: String,
      name: String,
      country: String,
      state: String,
      city: String,
      neighbourhoodCleansed: String,
      latitude: Double,
      longitude: Double,
      propertyType: String,
      roomType: String,
      accommodates: Int,
      bathrooms: Double,
      bedrooms: Int,
      amenities: Set[String] = Set.empty,
      price: Int,
      minimumNights: Int,
      maximumNights: Int,
      availability365: Int,
      hosts: Set[String] = Set.empty
  ) extends State {
    override def applyEvent(event: Event): State = event match {
      case ListingUpdated(listing) => listing
      case ListingRemoved          => Removed
      case HostAddedToListing(hostId) =>
        this.copy(hosts = this.hosts + hostId)
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [HostAddedToListing]"
        )
    }
  }
  final case object Removed extends State {
    override def applyEvent(event: Event): State = event match {
      case _ =>
        throw new IllegalStateException(
          "Not persist events allowed for a removed Listing"
        )
    }
  }

  sealed trait Command
  case class CreateListing(
      id: String,
      name: String,
      country: String,
      state: String,
      city: String,
      neighbourhoodCleansed: String,
      latitude: Double,
      longitude: Double,
      propertyType: String,
      roomType: String,
      accommodates: Int,
      bathrooms: Double,
      bedrooms: Int,
      amenities: Set[String] = Set.empty,
      price: Int,
      minimumNights: Int,
      maximumNights: Int,
      availability365: Int,
      replyTo: ActorRef[Response]
  ) extends Command
  case class UpdateListing(
      name: String,
      country: String,
      state: String,
      city: String,
      neighbourhoodCleansed: String,
      latitude: Double,
      longitude: Double,
      propertyType: String,
      roomType: String,
      accommodates: Int,
      bathrooms: Double,
      bedrooms: Int,
      amenities: Set[String],
      price: Int,
      minimumNights: Int,
      maximumNights: Int,
      availability365: Int,
      replyTo: ActorRef[Response]
  ) extends Command
  case class GetListing(id: String, replyTo: ActorRef[Response]) extends Command
  case class RemoveListing(replyTo: ActorRef[Response]) extends Command
  case class AddHostToListing(
      id: String,
      hostId: String,
      replyTo: ActorRef[Response]
  ) extends Command

  sealed trait Event extends CborSerializable
  case class ListingCreated(listing: Listing) extends Event
  case class ListingUpdated(listing: Listing) extends Event
  case object ListingRemoved extends Event
  case class HostAddedToListing(hostId: String) extends Event

  sealed trait Response
  case class ListingCreatedResponse(listing: Try[Listing]) extends Response
  case class ListingUpdatedResponse(listing: Try[Done]) extends Response
  case class GetListingResponse(listing: Option[Listing]) extends Response
  case class ListingDeletedResponse(listing: Try[Done]) extends Response
  case object AlreadyDeletedResponse extends Response
  case class HostAddedToListingResponse(listing: Try[Listing]) extends Response

  private def handleCommand(
      context: ActorContext[_]
  ): (State, Command) => ReplyEffect[Event, State] = (state, command) =>
    state match {
      case EmptyListing =>
        command match {
          case cmd: CreateListing =>
            val listingState = Listing(
              cmd.id,
              cmd.name,
              cmd.country,
              cmd.state,
              cmd.city,
              cmd.neighbourhoodCleansed,
              cmd.latitude,
              cmd.longitude,
              cmd.propertyType,
              cmd.roomType,
              cmd.accommodates,
              cmd.bathrooms,
              cmd.bedrooms,
              cmd.amenities,
              cmd.price,
              cmd.minimumNights,
              cmd.maximumNights,
              cmd.availability365
            )
            Effect
              .persist(ListingCreated(listingState))
              .thenReply(cmd.replyTo) { _ =>
                context.log.info(
                  s"The id is: ${cmd.id} and the name is: ${cmd.name}"
                )
                ListingCreatedResponse(Success(listingState))
              }
          case cmd: GetListing =>
            Effect.reply(cmd.replyTo)(GetListingResponse(None))
          case cmd: UpdateListing =>
            Effect.reply(cmd.replyTo) {
              ListingUpdatedResponse(
                Failure(
                  new RuntimeException("Listing has not been created yet")
                )
              )
            }
          case cmd: AddHostToListing =>
            Effect.reply(cmd.replyTo) {
              HostAddedToListingResponse(
                Failure(new RuntimeException("Listing doesn't exist"))
              )
            }
          case cmd: RemoveListing =>
            context.log.info(s"Listing doesn't exist")
            Effect.reply(cmd.replyTo) {
              ListingDeletedResponse(
                Failure(new RuntimeException("Listing doesn't exist"))
              )
            }
        }
      case state: Listing =>
        command match {
          case cmd: UpdateListing =>
            val listing = Listing(
              state.id,
              cmd.name,
              cmd.country,
              cmd.state,
              cmd.city,
              cmd.neighbourhoodCleansed,
              cmd.latitude,
              cmd.longitude,
              cmd.propertyType,
              cmd.roomType,
              cmd.accommodates,
              cmd.bathrooms,
              cmd.bedrooms,
              cmd.amenities,
              cmd.price,
              cmd.minimumNights,
              cmd.maximumNights,
              cmd.availability365,
              state.hosts
            )
            Effect
              .persist(ListingUpdated(listing))
              .thenReply(cmd.replyTo) { _ =>
                context.log.info(s"Listing with id: ${state.id} updated")
                ListingUpdatedResponse(Success(Done))
              }
          case cmd: GetListing =>
            Effect.reply(cmd.replyTo)(GetListingResponse(Some(state)))
          case cmd: AddHostToListing =>
            Effect
              .persist(HostAddedToListing(cmd.hostId))
              .thenReply(cmd.replyTo)(newState =>
                HostAddedToListingResponse(
                  Success(newState.asInstanceOf[Listing])
                )
              )
          case cmd: RemoveListing =>
            context.log.info(s"Listing with id: ${state.id} has been removed")
            Effect.persist(ListingRemoved).thenReply(cmd.replyTo) { _ =>
              ListingDeletedResponse(Success(Done))
            }
          case cmd: CreateListing =>
            Effect.reply(cmd.replyTo)(
              ListingCreatedResponse(
                Failure(new RuntimeException("Listing already created"))
              )
            )
        }
      case Removed =>
        command match {
          case cmd: GetListing =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: CreateListing =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: UpdateListing =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: AddHostToListing =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: RemoveListing =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
        }

    }

  private def handleEvent: (State, Event) => State = (state, event) =>
    state.applyEvent(event)

  def apply(persistenceId: PersistenceId): Behavior[Command] = Behaviors.setup {
    context =>
      EventSourcedBehavior
        .withEnforcedReplies[Command, Event, State](
          persistenceId = persistenceId,
          emptyState = EmptyListing,
          commandHandler = handleCommand(context),
          eventHandler = handleEvent
        )
        .withRetention(
          RetentionCriteria
            .snapshotEvery(numberOfEvents = 100, keepNSnapshots = 3)
        )
        .onPersistFailure(
          SupervisorStrategy.restartWithBackoff(1000.millis, 60.second, 0.1)
        )
  }

}
