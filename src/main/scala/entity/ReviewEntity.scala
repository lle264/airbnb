package entity

import java.time.LocalDate
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

import akka.Done
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{
  ClusterSharding,
  Entity,
  EntityTypeKey
}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{
  Effect,
  EventSourcedBehavior,
  ReplyEffect,
  RetentionCriteria
}

object ReviewEntity {

  val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("review")

  def initSharding(system: ActorSystem[_]): Unit =
    ClusterSharding(system).init(Entity(TypeKey) { entityContext =>
      ReviewEntity(
        PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
      )
    })

  sealed trait State extends CborSerializable {
    def applyEvent(event: Event): State
  }
  final case object EmptyReview extends State {
    override def applyEvent(event: Event): State = event match {
      case ReviewAdded(review) => review
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [EmptyReview]"
        )
    }
  }
  final case class Review(
      id: String,
      listingId: String,
      reviewerId: String,
      reviewerName: String,
      comment: String,
      date: LocalDate
  ) extends State {
    override def applyEvent(event: Event): State = event match {
      case ReviewUpdated(review) => review
      case ReviewRemoved         => Removed
      case _ =>
        throw new IllegalStateException(
          s"unexpected event [$event] in state [Review]"
        )
    }
  }
  final case object Removed extends State {
    override def applyEvent(event: Event): State = event match {
      case _ =>
        throw new IllegalStateException(
          "Not persist events allowed for a removed Review"
        )
    }
  }

  sealed trait Command
  final case class AddReview(
      id: String,
      listingId: String,
      reviewerId: String,
      reviewerName: String,
      comment: String,
      replyTo: ActorRef[Response]
  ) extends Command
  final case class UpdateReview(comment: String, replyTo: ActorRef[Response])
      extends Command
  final case class GetReview(id: String, replyTo: ActorRef[Response])
      extends Command
  final case class RemoveReview(replyTo: ActorRef[Response]) extends Command

  sealed trait Event extends CborSerializable
  final case class ReviewAdded(review: Review) extends Event
  final case class ReviewUpdated(review: Review) extends Event
  final case object ReviewRemoved extends Event

  sealed trait Response
  final case class ReviewAddedResponse(review: Try[Review]) extends Response
  final case class ReviewUpdatedResponse(done: Try[Done]) extends Response
  final case class GetReviewResponse(review: Option[Review]) extends Response
  final case class ReviewDeletedResponse(done: Try[Done]) extends Response
  final case object AlreadyDeletedResponse extends Response

  private def handleCommand(
      context: ActorContext[_]
  ): (State, Command) => ReplyEffect[Event, State] = (state, command) =>
    state match {
      case EmptyReview =>
        command match {
          case cmd: AddReview =>
            val review = Review(
              cmd.id,
              cmd.listingId,
              cmd.reviewerId,
              cmd.reviewerName,
              cmd.comment,
              LocalDate.now()
            )
            Effect
              .persist(ReviewAdded(review))
              .thenReply(cmd.replyTo)(_ => ReviewAddedResponse(Success(review)))
          case cmd: UpdateReview =>
            Effect.reply(cmd.replyTo) {
              ReviewUpdatedResponse(
                Failure(new RuntimeException("Review has not been created yet"))
              )
            }
          case cmd: GetReview =>
            Effect.reply(cmd.replyTo)(GetReviewResponse(None))
          case cmd: RemoveReview =>
            Effect.reply(cmd.replyTo)(
              ReviewDeletedResponse(
                Failure(new RuntimeException("Review doesn't exist"))
              )
            )
        }
      case state: Review =>
        command match {
          case cmd: GetReview =>
            Effect.reply(cmd.replyTo)(GetReviewResponse(Some(state)))
          case cmd: UpdateReview =>
            val review = state.copy(comment = cmd.comment)
            Effect
              .persist(ReviewUpdated(review))
              .thenReply(cmd.replyTo)(_ => ReviewUpdatedResponse(Success(Done)))
          case cmd: RemoveReview =>
            context.log.info(s"Review with id: ${state.id} has been removed")
            Effect
              .persist(ReviewRemoved)
              .thenReply(cmd.replyTo)(_ => ReviewDeletedResponse(Success(Done)))
          case cmd: AddReview =>
            Effect.reply(cmd.replyTo)(
              ReviewAddedResponse(
                Failure(new RuntimeException("Review already added"))
              )
            )
        }
      case Removed =>
        command match {
          case cmd: GetReview =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: AddReview =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: UpdateReview =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
          case cmd: RemoveReview =>
            Effect.reply(cmd.replyTo)(AlreadyDeletedResponse)
        }
    }

  private def handleEvent: (State, Event) => State = (state, event) =>
    state.applyEvent(event)

  def apply(persistenceId: PersistenceId): Behavior[Command] = Behaviors.setup {
    context =>
      EventSourcedBehavior
        .withEnforcedReplies[Command, Event, State](
          persistenceId = persistenceId,
          emptyState = EmptyReview,
          commandHandler = handleCommand(context),
          eventHandler = handleEvent
        )
        .withRetention(
          RetentionCriteria
            .snapshotEvery(numberOfEvents = 100, keepNSnapshots = 3)
        )
        .onPersistFailure(
          SupervisorStrategy.restartWithBackoff(200.millis, 10.second, 0.1)
        )
  }

}
